package com.example.rbertoli.combustivel;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener{

    private Button mBtn01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtn01 = (Button) findViewById(R.id.btn01);





        mBtn01.setOnClickListener(new View.OnClickListener() {

                                      public void onClick(View v) {
                                          EditText fuel1 = (EditText) findViewById(R.id.fuel1);
                                          EditText fuel2 = (EditText) findViewById(R.id.fuel2);
                                          Float f1 = Float.valueOf(fuel1.getText().toString());
                                          Float f2 = Float.valueOf(fuel2.getText().toString());
                                          String resultado = null;



                                          float razao = f2/f1 ;

                                          if (razao < 0.7 ) {
                                              resultado  = "Etanol";
                                          }
                                          else {
                                              resultado = "Gasolina";
                                          }

                                          Intent intent = new Intent(MainActivity.this, Tela2.class);
                                          //Essa linha adiciona um parametro extra na intent
                                          intent.putExtra("resultado",resultado);
                                          startActivity(intent);
                                      }
                                  });


            mBtn01.setOnClickListener(this);


    }
    @Override
    protected void onResume() {
       // Logger.v("OnResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
     //   Logger.v("OnPause");

    }

    @Override
    protected void onStop() {
      //  Logger.v("OnStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
     //   Logger.v("OnDestroy");
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        EditText fuel1 = (EditText) findViewById(R.id.fuel1);
        EditText fuel2 = (EditText) findViewById(R.id.fuel2);
        Float f1 = Float.valueOf(fuel1.getText().toString());
        Float f2 = Float.valueOf(fuel2.getText().toString());
        String resultado = null;



        float razao = f2/f1 ;

        if (razao < 0.7 ) {
            //resultado  = "Etanol";
            resultado = getResources().getString(R.string.etanol);
        }
        else {
            //resultado = "Gasolina";
            resultado = getResources().getString(R.string.gasolina);
        }

        Intent intent = new Intent(this, Tela2.class);
        //Essa linha adiciona um parametro extra na intent
        intent.putExtra("resultado",resultado);
        startActivity(intent);
    }

}
