package com.example.rbertoli.combustivel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by rbertoli on 18/03/17.
 */

public class Tela2 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela2);

        // Log.v(Constants.APP_LOG_TAG, "oncreate - Tela 2");

        Intent it = getIntent();

        if (it != null) {
            String param = it.getStringExtra("resultado");
            String message = getResources().getString(R.string.econo);
            Toast.makeText(this, message + " " + param, Toast.LENGTH_LONG).show();
            //finish();
        }
    }
}
